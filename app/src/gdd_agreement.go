package main

import (
	"fmt"
	"os"
	"packages/agreement"
)

func main() {
	workingDirectory, orcaOutputDirectory, inputListFileName, outputJSONFileName := setParameters()
	a := agreement.NewAgreementFromOrca(workingDirectory, orcaOutputDirectory, inputListFileName)
	a.Start()
	a.Compare_all_networks(outputJSONFileName)
}

func setParameters() (string, string, string, string) {
	args := os.Args
	if len(args) < 5 {
		fmt.Println("Please, enter 4 file names as executable program arguments:" +
			"\n - working directory" +
			"\n - directory with orca output" +
			"\n - gdd agreement input file (list of compared files)" +
			"\n - result JSON output file")
		os.Exit(0)
	}
	return args[1], args[2], args[3], args[4]
}
