package main

import (
	"bufio"
	"fmt"
	"packages/a_u_x"
	"math/big"
	"os"
)

func main() {
	var prec uint = 1200
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Println("Welcome to nth root of x calculator")
		fmt.Println("set n:")
		n_input, _ := reader.ReadString('\n')
		fmt.Println("set x:")
		x_input, _ := reader.ReadString('\n')

		n, _, _ := big.ParseFloat(n_input, 10, prec, big.ToNearestEven)
		x, _, _ := big.ParseFloat(x_input, 10, prec, big.ToNearestEven)
		root := a_u_x.Big_float_nth_root(n, x, prec)

		fmt.Println(n, "th root of", x, "=")
		fmt.Println(root)
		fmt.Println()
	}
}
