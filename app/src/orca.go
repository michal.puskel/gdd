package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Graph struct {
	nodes       []map[int]bool
	cnPairs     map[[2]int]map[int]bool
	cncTriplets map[[3]int]map[int]bool
	outFileName string
	orbit       [][73]uint64
}

func loadFile() *Graph {
	args := os.Args
	if len(args) < 3 {
		fmt.Println("Please, enter 2 file names as executable program arguments:" +
			"\n - input graph file" +
			"\n - result output file")
		os.Exit(0)
	}
	inFileName, outFileName := args[1], args[2]

	inFile, err := os.Open(inFileName)
	defer inFile.Close()
	if err != nil {
		panic(err)
	}

	graph := new(Graph)
	graph.cnPairs = make(map[[2]int]map[int]bool)
	graph.cncTriplets = make(map[[3]int]map[int]bool)
	graph.outFileName = outFileName

	br := bufio.NewReader(inFile)
	line, _, _ := br.ReadLine()
	graphAttributes := strings.Fields(string(line))
	nValue, _ := strconv.ParseInt(graphAttributes[0], 10, 32)
	eValue, _ := strconv.ParseInt(graphAttributes[1], 10, 32)
	n, e := int(nValue), int(eValue)
	graph.nodes = make([]map[int]bool, n)
	for i := 0; i < n; i++ {
		graph.nodes[i] = make(map[int]bool)

	}
	for i := 0; i < e; i++ {
		line, _, _ = br.ReadLine()
		edge := strings.Fields(string(line))
		n1Value, _ := strconv.ParseInt(edge[0], 10, 32)
		n2Value, _ := strconv.ParseInt(edge[1], 10, 32)
		n1, n2 := int(n1Value), int(n2Value)

		graph.nodes[n1][n2] = true
		graph.nodes[n2][n1] = true
	}

	return graph
}

func (g *Graph) countNeighboursSets() {
	// count pairs
	for u := range g.nodes {
		for v := range g.nodes {
			if u < v {
				for nbrU := range g.nodes[u] {
					if _, adjacentVNbrU := g.nodes[v][nbrU]; adjacentVNbrU {
						if _, initializedUV := g.cnPairs[[2]int{u, v}]; !initializedUV {
							g.cnPairs[[2]int{u, v}] = make(map[int]bool)
						}
						g.cnPairs[[2]int{u, v}][nbrU] = true
					}
				}
			}
		}
	}
	// count triplets
	for u := range g.nodes {
		for v := range g.nodes[u] {
			if u < v {
				for t := range g.nodes[u] {
					if _, adjacentVT := g.nodes[v][t]; adjacentVT || v != t {
						for nbrT := range g.nodes[t] {
							g.countTripletNeighbours(u, v, t, nbrT)
						}
					}
				}
				for t := range g.nodes[v] {
					if _, adjacentUT := g.nodes[u][t]; adjacentUT || u != t {
						for nbrT := range g.nodes[t] {
							g.countTripletNeighbours(u, v, t, nbrT)
						}
					}
				}
			}
		}
	}
}

func (g *Graph) countTripletNeighbours(u, v, t, nbrT int) {
	adjacentUNbrT, adjacentVNbrT := g.nodes[u][nbrT], g.nodes[v][nbrT]

	if adjacentUNbrT && adjacentVNbrT {
		if _, initializedUVT := g.cncTriplets[g.sort3(u, v, t)]; !initializedUVT {
			g.cncTriplets[g.sort3(u, v, t)] = make(map[int]bool)
		}
		g.cncTriplets[g.sort3(u, v, t)][nbrT] = true
	}
}

func (g *Graph) countCnPairs(u, v int) uint64 {
	var uu, vv int
	if u < v {
		uu, vv = u, v
	} else {
		uu, vv = v, u
	}
	return uint64(len(g.cnPairs[[2]int{uu, vv}]))
}

func (g *Graph) sort3(u, v, t int) [3]int {
	if u < v {
		if u < t {
			if v < t {
				return [3]int{u, v, t}
			}
			return [3]int{u, t, v}
		}
		return [3]int{t, u, v}
	}
	if v < t {
		if u < t {
			return [3]int{v, u, t}
		}
		return [3]int{v, t, u}
	}
	return [3]int{t, v, u}
}

func (g *Graph) countCncTriplets(u, v, t int) uint64 {
	return uint64(len(g.cncTriplets[g.sort3(u, v, t)]))
}

func (g *Graph) countFormulas() {
	o := make([][73]uint64, len(g.nodes))

	for x := range g.nodes {
		var fr [72]uint64

		o[x][0] = uint64(len(g.nodes[x]))

		for u := range g.nodes[x] {
			for v := range g.nodes[u] {
				if _, adjacentXV := g.nodes[x][v]; adjacentXV {
					if u < v {
						o[x][3]++
					}

					for t := range g.nodes {
						adjacentXT, adjacentUT, adjacentVT := g.nodes[x][t], g.nodes[u][t], g.nodes[v][t]

						switch {
						case adjacentXT && adjacentUT && adjacentVT && u < v && v < t:
							o[x][14]++

							fr[71] += g.countCncTriplets(x, u, v) - 1 + g.countCncTriplets(x, u, t) - 1 + g.countCncTriplets(x, v, t) - 1
							fr[70] += g.countCncTriplets(u, v, t) - 1
							fr[67] += g.countCnPairs(x, u) - 2 + g.countCnPairs(x, v) - 2 + g.countCnPairs(x, t) - 2
							fr[66] += g.countCnPairs(u, v) - 2 + g.countCnPairs(u, t) - 2 + g.countCnPairs(v, t) - 2
							fr[58] += uint64(len(g.nodes[x])) - 3
							fr[57] += uint64(len(g.nodes[u])) - 3 + uint64(len(g.nodes[v])) - 3 + uint64(len(g.nodes[t])) - 3

							for w := range g.nodes[t] {
								adjacentXW, adjacentUW, adjacentVW := g.nodes[x][w], g.nodes[u][w], g.nodes[v][w]

								if adjacentXW && adjacentUW && adjacentVW && t < w {
									o[x][72]++
								}
							}

						case adjacentXT && adjacentUT && !adjacentVT && v < t:
							o[x][13]++

							fr[69] += g.countCncTriplets(x, v, t) - 1
							fr[68] += g.countCncTriplets(u, v, t) - 1
							fr[64] += g.countCnPairs(v, t) - 2
							fr[61] += g.countCnPairs(x, v) - 1 + g.countCnPairs(x, t) - 1
							fr[60] += g.countCnPairs(u, v) - 1 + g.countCnPairs(u, t) - 1
							fr[55] += g.countCnPairs(x, u) - 2
							fr[48] += uint64(len(g.nodes[v])) - 2 + uint64(len(g.nodes[t])) - 2
							fr[42] += uint64(len(g.nodes[x])) - 3
							fr[41] += uint64(len(g.nodes[u])) - 3

						case !adjacentXT && adjacentUT && adjacentVT && u < v && x != t:
							o[x][12]++

							fr[65] += g.countCncTriplets(u, v, t)
							fr[63] += g.countCnPairs(x, t) - 2
							fr[59] += g.countCnPairs(u, t) - 1 + g.countCnPairs(v, t) - 1
							fr[54] += g.countCnPairs(u, v) - 2
							fr[47] += uint64(len(g.nodes[x])) - 2
							fr[46] += uint64(len(g.nodes[t])) - 2
							fr[40] += uint64(len(g.nodes[u])) - 3 + uint64(len(g.nodes[v])) - 3

						case adjacentXT && !adjacentUT && !adjacentVT && u < v:
							o[x][11]++

							fr[44] += g.countCnPairs(x, t)
							fr[33] += uint64(len(g.nodes[x])) - 3
							fr[30] += uint64(len(g.nodes[t])) - 1
							fr[26] += uint64(len(g.nodes[u])) - 2 + uint64(len(g.nodes[v])) - 2

						case !adjacentXT && !adjacentUT && adjacentVT && x != t && u != t:
							o[x][10]++

							fr[52] += g.countCnPairs(u, t) - 1
							fr[43] += g.countCnPairs(v, t)
							fr[32] += uint64(len(g.nodes[v])) - 3
							fr[29] += uint64(len(g.nodes[t])) - 1
							fr[25] += uint64(len(g.nodes[u])) - 2
						}
					}
				} else if x != v {
					o[x][1]++

					for t := range g.nodes[u] {
						if x != t {
							adjacentXT, adjacentVT := g.nodes[x][t], g.nodes[v][t]

							if !adjacentXT && v < t {
								if adjacentVT {
									o[x][9]++

									fr[56] += g.countCncTriplets(u, v, t)
									fr[45] += g.countCnPairs(v, t) - 1
									fr[39] += g.countCnPairs(u, v) - 1 + g.countCnPairs(u, t) - 1
									fr[31] += uint64(len(g.nodes[u])) - 3
									fr[28] += uint64(len(g.nodes[x])) - 1
									fr[24] += uint64(len(g.nodes[v])) - 2 + uint64(len(g.nodes[t])) - 2
								} else {
									o[x][6]++

									fr[22] += uint64(len(g.nodes[u])) - 3
									fr[20] += uint64(len(g.nodes[x])) - 1
									fr[19] += uint64(len(g.nodes[v])) - 1 + uint64(len(g.nodes[t])) - 1
								}
							}
						}
					}

					for t := range g.nodes[v] {
						adjacentXT, adjacentUT := g.nodes[x][t], g.nodes[u][t]

						if !adjacentXT && !adjacentUT && u != t {
							o[x][4]++

							fr[35] += g.countCnPairs(u, t) - 1
							fr[34] += g.countCnPairs(x, t)
							fr[27] += g.countCnPairs(v, t)
							fr[18] += uint64(len(g.nodes[v])) - 2
							fr[16] += uint64(len(g.nodes[x])) - 1
							fr[15] += uint64(len(g.nodes[t])) - 1
						}
					}
				}
			}

			for v := range g.nodes[x] {
				if _, adjacentUV := g.nodes[u][v]; !adjacentUV && u != v {
					if u < v {
						o[x][2]++

						for t := range g.nodes[u] {
							adjacentXT, adjacentVT := g.nodes[x][t], g.nodes[v][t]

							if !adjacentXT && adjacentVT && x != t {
								o[x][8]++

								fr[62] += g.countCncTriplets(u, v, t)
								fr[53] += g.countCnPairs(x, u) + g.countCnPairs(x, v)
								fr[51] += g.countCnPairs(u, t) + g.countCnPairs(t, v)
								fr[50] += g.countCnPairs(x, t) - 2
								fr[49] += g.countCnPairs(u, v) - 2
								fr[38] += uint64(len(g.nodes[x])) - 2
								fr[37] += uint64(len(g.nodes[u])) - 2 + uint64(len(g.nodes[v])) - 2
								fr[36] += uint64(len(g.nodes[t])) - 2
							}
						}

						for t := range g.nodes[x] {
							adjacentUT, adjacentVT := g.nodes[u][t], g.nodes[v][t]

							if !adjacentUT && !adjacentVT && v < t {
								o[x][7]++

								fr[23] += uint64(len(g.nodes[x])) - 3
								fr[21] += uint64(len(g.nodes[u])) - 1 + uint64(len(g.nodes[v])) - 1 + uint64(len(g.nodes[t])) - 1
							}
						}
					}

					for t := range g.nodes[v] {
						adjacentXT, adjacentUT := g.nodes[x][t], g.nodes[u][t]

						if !adjacentXT && !adjacentUT && x != t {
							o[x][5]++

							fr[17] += uint64(len(g.nodes[u])) - 1
						}
					}
				}
			}
		}

		// when solving equations for specific 'x', all enumerations have to be finished before solving

		// solve equations
		o[x][71] = (fr[71] - 12*o[x][72]) / 2
		o[x][70] = fr[70] - 4*o[x][72]
		o[x][69] = (fr[69] - 2*o[x][71]) / 4
		o[x][68] = fr[68] - 2*o[x][71]
		o[x][67] = fr[67] - 12*o[x][72] - 4*o[x][71]
		o[x][66] = fr[66] - 12*o[x][72] - 2*o[x][71] - 3*o[x][70]
		o[x][65] = (fr[65] - 3*o[x][70]) / 2
		o[x][64] = fr[64] - 2*o[x][71] - 4*o[x][69] - o[x][68]
		o[x][63] = fr[63] - 3*o[x][70] - 2*o[x][68]
		o[x][62] = (fr[62] - o[x][68]) / 2
		o[x][61] = (fr[61] - 4*o[x][71] - 8*o[x][69] - 2*o[x][67]) / 2
		o[x][60] = fr[60] - 4*o[x][71] - 2*o[x][68] - 2*o[x][67]
		o[x][59] = fr[59] - 6*o[x][70] - 2*o[x][68] - 4*o[x][65]
		o[x][58] = fr[58] - 4*o[x][72] - 2*o[x][71] - o[x][67]
		o[x][57] = fr[57] - 12*o[x][72] - 4*o[x][71] - 3*o[x][70] - o[x][67] - 2*o[x][66]
		o[x][56] = (fr[56] - 2*o[x][65]) / 3
		o[x][55] = (fr[55] - 2*o[x][71] - 2*o[x][67]) / 3
		o[x][54] = (fr[54] - 3*o[x][70] - o[x][66] - 2*o[x][65]) / 2
		o[x][53] = fr[53] - 2*o[x][68] - 2*o[x][64] - 2*o[x][63]
		o[x][52] = (fr[52] - 2*o[x][66] - 2*o[x][64] - o[x][59]) / 2
		o[x][51] = fr[51] - 2*o[x][68] - 2*o[x][63] - 4*o[x][62]
		o[x][50] = (fr[50] - o[x][68] - 2*o[x][63]) / 3
		o[x][49] = (fr[49] - o[x][68] - o[x][64] - 2*o[x][62]) / 2
		o[x][48] = fr[48] - 4*o[x][71] - 8*o[x][69] - 2*o[x][68] - 2*o[x][67] - 2*o[x][64] - 2*o[x][61] - o[x][60]
		o[x][47] = fr[47] - 3*o[x][70] - 2*o[x][68] - o[x][66] - o[x][63] - o[x][60]
		o[x][46] = fr[46] - 3*o[x][70] - 2*o[x][68] - 2*o[x][65] - o[x][63] - o[x][59]
		o[x][45] = fr[45] - 2*o[x][65] - 2*o[x][62] - 3*o[x][56]
		o[x][44] = (fr[44] - o[x][67] - 2*o[x][61]) / 4
		o[x][43] = (fr[43] - 2*o[x][66] - o[x][60] - o[x][59]) / 2
		o[x][42] = fr[42] - 2*o[x][71] - 4*o[x][69] - 2*o[x][67] - 2*o[x][61] - 3*o[x][55]
		o[x][41] = fr[41] - 2*o[x][71] - o[x][68] - 2*o[x][67] - o[x][60] - 3*o[x][55]
		o[x][40] = fr[40] - 6*o[x][70] - 2*o[x][68] - 2*o[x][66] - 4*o[x][65] - o[x][60] - o[x][59] - 4*o[x][54]
		o[x][39] = (fr[39] - 4*o[x][65] - o[x][59] - 6*o[x][56]) / 2
		o[x][38] = fr[38] - o[x][68] - o[x][64] - 2*o[x][63] - o[x][53] - 3*o[x][50]
		o[x][37] = fr[37] - 2*o[x][68] - 2*o[x][64] - 2*o[x][63] - 4*o[x][62] - o[x][53] - o[x][51] - 4*o[x][49]
		o[x][36] = fr[36] - o[x][68] - 2*o[x][63] - 2*o[x][62] - o[x][51] - 3*o[x][50]
		o[x][35] = (fr[35] - o[x][59] - 2*o[x][52] - 2*o[x][45]) / 2
		o[x][34] = (fr[34] - o[x][59] - 2*o[x][52] - o[x][51]) / 2
		o[x][33] = (fr[33] - o[x][67] - 2*o[x][61] - 3*o[x][58] - 4*o[x][44] - 2*o[x][42]) / 2
		o[x][32] = (fr[32] - 2*o[x][66] - o[x][60] - o[x][59] - 2*o[x][57] - 2*o[x][43] - 2*o[x][41] - o[x][40]) / 2
		o[x][31] = fr[31] - 2*o[x][65] - o[x][59] - 3*o[x][56] - o[x][43] - 2*o[x][39]
		o[x][30] = fr[30] - o[x][67] - o[x][63] - 2*o[x][61] - o[x][53] - 4*o[x][44]
		o[x][29] = fr[29] - 2*o[x][66] - 2*o[x][64] - o[x][60] - o[x][59] - o[x][53] - 2*o[x][52] - 2*o[x][43]
		o[x][28] = fr[28] - 2*o[x][65] - 2*o[x][62] - o[x][59] - o[x][51] - o[x][43]
		o[x][27] = (fr[27] - o[x][59] - o[x][51] - 2*o[x][45]) / 2
		o[x][26] = fr[26] - 2*o[x][67] - 2*o[x][63] - 2*o[x][61] - 6*o[x][58] - o[x][53] - 2*o[x][47] - 2*o[x][42]
		o[x][25] = (fr[25] - 2*o[x][66] - 2*o[x][64] - o[x][59] - 2*o[x][57] - 2*o[x][52] - o[x][48] - o[x][40]) / 2
		o[x][24] = fr[24] - 4*o[x][65] - 4*o[x][62] - o[x][59] - 6*o[x][56] - o[x][51] - 2*o[x][45] - 2*o[x][39]
		o[x][23] = (fr[23] - o[x][55] - o[x][42] - 2*o[x][33]) / 4
		o[x][22] = (fr[22] - 2*o[x][54] - o[x][40] - o[x][39] - o[x][32] - 2*o[x][31]) / 3
		o[x][21] = fr[21] - 3*o[x][55] - 3*o[x][50] - 2*o[x][42] - 2*o[x][38] - 2*o[x][33]
		o[x][20] = fr[20] - 2*o[x][54] - 2*o[x][49] - o[x][40] - o[x][37] - o[x][32]
		o[x][19] = fr[19] - 4*o[x][54] - 4*o[x][49] - o[x][40] - 2*o[x][39] - o[x][37] - 2*o[x][35] - 2*o[x][31]
		o[x][18] = (fr[18] - o[x][59] - o[x][51] - 2*o[x][46] - 2*o[x][45] - 2*o[x][36] - 2*o[x][27] - o[x][24]) / 2
		o[x][17] = (fr[17] - o[x][60] - o[x][53] - o[x][51] - o[x][48] - o[x][37] - 2*o[x][34] - 2*o[x][30]) / 2
		o[x][16] = fr[16] - o[x][59] - 2*o[x][52] - o[x][51] - 2*o[x][46] - 2*o[x][36] - 2*o[x][34] - o[x][29]
		o[x][15] = fr[15] - o[x][59] - 2*o[x][52] - o[x][51] - 2*o[x][45] - 2*o[x][35] - 2*o[x][34] - 2*o[x][27]
	}

	g.orbit = o
}

func (g *Graph) writeResults() {
	// delete existing old file
	if _, err := os.Stat(g.outFileName); !os.IsNotExist(err) {
		err := os.Remove(g.outFileName)
		if err != nil {
			panic(err)
		}
	}

	// create new file
	outFile, err := os.Create(g.outFileName)
	defer outFile.Close()
	if err != nil {
		panic(err)
	}

	// write results
	for x := range g.nodes {
		outFile.WriteString(strings.Trim(fmt.Sprint(g.orbit[x]), "[]") + "\n")
	}
	outFile.Sync()
}

func main() {
	g := loadFile()
	g.countNeighboursSets()
	g.countFormulas()
	g.writeResults()
}
