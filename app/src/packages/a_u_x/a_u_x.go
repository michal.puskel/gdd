package a_u_x

import (
	"fmt"
	// "log"
	"math/big"
	"os"
	"sort"
	"strconv"
	"strings"
)

const Hausnum_prec uint = 1200 // 100000

func Check_error(e error) {
	if e != nil {
		panic(e)
	}
}

// beware of overflow
func Comb(n, k int) int {
	if n == k || k == 0 {
		return 1
	}
	if k > n {
		panic("Math error in combinatorial number: k > n")
	}

	numerator := 1
	for i := n; i > n-k; i-- {
		numerator *= i
	}

	return numerator / (Fac(k))
}

// beware of overflow
func Fac(x int) int {
	fac := 1
	for i := 1; i <= x; i++ {
		fac *= i
	}
	return fac
}

func Min(x, y uint64) uint64 {
	if x < y {
		return x
	}
	return y
}

func Max(x, y uint64) uint64 {
	if x > y {
		return x
	}
	return y
}

func Min_int(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func Max_int(x, y int) int {
	if x > y {
		return x
	}
	return y
}

type SortAbleUInt64Array []uint64

func (a SortAbleUInt64Array) Len() int {
	return len(a)
}

func (a SortAbleUInt64Array) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a SortAbleUInt64Array) Less(i, j int) bool {
	return a[i] < a[j]
}

type SortAbleStringArray []string

func (a SortAbleStringArray) Len() int {
	return len(a)
}

func (a SortAbleStringArray) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a SortAbleStringArray) Less(i, j int) bool {
	return a[i] < a[j]
}

func Convert_uint64_to_int64(unsigned_int_64 uint64) int64 {
	var signed_int_64 int64 = int64(unsigned_int_64)
	if signed_int_64 < 0 {
		panic(fmt.Sprintf("Signed int 64 overflow error uint64 [%v] ->  int64 [%v]\n", unsigned_int_64, signed_int_64))
		os.Exit(1)
	}
	return signed_int_64
}

func Convert_uint_to_int(unsigned_int uint) int {
	var signed_int int = int(unsigned_int)
	if signed_int < 0 {
		panic(fmt.Sprintf("Signed int overflow error uint [%v] ->  int [%v]\n", unsigned_int, signed_int))
		os.Exit(1)
	}
	return signed_int
}

func Convert_int64_to_float64(signed_int_64 int64) float64 {
	var signed_float_64 float64 = float64(signed_int_64)

	diff := signed_int_64 - int64(signed_float_64)
	if diff != 0 {
		panic(fmt.Sprintf("Signed float 64 overflow error int64 [%v] -> float64 [%v]\n", signed_int_64, signed_float_64))
		os.Exit(1)
	}

	return signed_float_64
}

func Convert_int64_to_int32(signed_int_64 int64) int {
	var signed_int_32 int = int(signed_int_64)

	diff := signed_int_64 - int64(signed_int_32)
	if diff != 0 {
		panic(fmt.Sprintf("Signed int 32 overflow error int64 [%v] -> int32 [%v]\n", signed_int_64, signed_int_32))
		os.Exit(1)
	}

	return signed_int_32
}

// integer n_big >= 0
// float x_big_float >= 0
func Big_float_nth_root(n_big, x_big_float *big.Float, prec uint) *big.Float {
	if n_big.Signbit() {
		panic(fmt.Sprintf("n_big is negative %v\n", n_big))
		os.Exit(1)
	}
	if x_big_float.Signbit() {
		panic(fmt.Sprintf("x_big_float is negative %v\n", x_big_float))
		os.Exit(1)
	}

	// big.NewFloat() allowed here: begin
	if x_big_float.Cmp(big.NewFloat(0)) == 0 {
		return big.NewFloat(0)
	}

	if x_big_float.Cmp(big.NewFloat(1)) == 0 {
		return big.NewFloat(1)
	}

	if n_big.Cmp(big.NewFloat(0)) == 0 {
		return big.NewFloat(1)
	}

	if n_big.Cmp(big.NewFloat(1)) == 0 {
		return x_big_float
	}
	// big.NewFloat() allowed here: end

	// get_aligned_blocks
	string_real_x := x_big_float.Text('f', Convert_uint_to_int(prec))

	check_real_string(string_real_x)
	ix_decimal_point, check := find_character(string_real_x, '.')
	if !check {
		panic(fmt.Sprintf("string of real number not proper format error: no '.' decimal point in string (%v)\n", string_real_x))
		os.Exit(1)
	}

	len_integer_part := ix_decimal_point
	len_decimal_part := len(string_real_x) - 1 - ix_decimal_point

	len_integer_part_aligned, len_decimal_part_aligned := len_integer_part, len_decimal_part

	if !n_big.IsInt() {
		panic(fmt.Sprintf("n is not integer, Big_float_nth_root error: (%v)\n", n_big))
		os.Exit(1)
	}

	nn := big.NewInt(0)
	var accuracy_test big.Accuracy
	_, accuracy_test = n_big.Int(nn)

	if accuracy_test != big.Exact {
		panic(fmt.Sprintf("accuracy_test for bigInt conversion not satisfied, error: (%v) vs (%v)\n", n_big, nn))
		os.Exit(1)
	}

	n := Convert_int64_to_int32(nn.Int64())

	for len_integer_part_aligned%n != 0 {
		len_integer_part_aligned += 1
	}
	for len_decimal_part_aligned%n != 0 {
		len_decimal_part_aligned += 1
	}

	var zeros_head []string
	zeros_head_range := len_integer_part_aligned - len_integer_part
	for i := 0; i < zeros_head_range; i++ {
		zeros_head = append(zeros_head, "0")
	}

	var zeros_tail []string
	zeros_tail_range := len_decimal_part_aligned - len_decimal_part
	for i := 0; i < zeros_tail_range; i++ {
		zeros_tail = append(zeros_tail, "0")
	}

	integer_part_array := append(zeros_head, strings.Split(string_real_x, "")[:ix_decimal_point]...)
	decimal_part_array := append(strings.Split(string_real_x, "")[ix_decimal_point+1:], zeros_tail...)

	/////////////////////////////////////////////////////////////////////////

	radicand := append(integer_part_array, decimal_part_array...)
	base := big.NewInt(10)
	x, y := big.NewInt(0), big.NewInt(0)

	for i := uint(0); i < prec; i++ {
		alpha := strings.Join(radicand[:Min_int(n, len(radicand))], "")
		radicand = radicand[Min_int(n, len(radicand)):]

		var xx *big.Int = new(big.Int)
		xx.Exp(base, nn, big.NewInt(0))
		xx.Mul(xx, x)

		if alpha == "" {
			alpha = "0"
		}
		alpha_big_int := new(big.Int)
		_, conversion_success := alpha_big_int.SetString(alpha, 10)
		if !conversion_success {
			panic(fmt.Sprintf("alpha conversion from string to big Int failed: %v\n", alpha))
			os.Exit(1)
		}

		xx.Add(xx, alpha_big_int)

		//	beta = 0
		//	while (B*y + beta+1)**n <= xx and beta+1 <= B-1:
		//		beta += 1

		beta := big.NewInt(0)

		tmp := big.NewInt(0)
		tmp.Mul(base, y)
		tmp2 := big.NewInt(0)
		tmp2.Add(beta, big.NewInt(1))
		tmp.Add(tmp, tmp2)
		tmp.Exp(tmp, nn, big.NewInt(0))

		tmp2.Add(beta, big.NewInt(1))

		var tmp3 *big.Int = new(big.Int)
		tmp3.Sub(base, big.NewInt(1))

		eq_l := tmp.Cmp(xx)
		eq_r := tmp2.Cmp(tmp3)

		for eq_l <= 0 && eq_r <= 0 {
			beta.Add(beta, big.NewInt(1))

			tmp.Mul(base, y)
			tmp2.Add(beta, big.NewInt(1))
			tmp.Add(tmp, tmp2)
			tmp.Exp(tmp, nn, big.NewInt(0))

			tmp2.Add(beta, big.NewInt(1))

			tmp3.Sub(base, big.NewInt(1))

			eq_l = tmp.Cmp(xx)
			eq_r = tmp2.Cmp(tmp3)
		}

		// end beta

		var yy *big.Int = new(big.Int)
		yy.Mul(base, y)
		yy.Add(yy, beta)

		x, y = xx, yy
	}

	nth_root := y.Text(10)
	nth_root = nth_root[:1] + "." + nth_root[1:]

	/////////////////////////////////////////////////////////////////////////

	extracted_root, base_check, err := big.ParseFloat(nth_root, 10, Hausnum_prec, big.ToNearestEven)
	if base_check != 10 {
		panic(fmt.Sprintf("base check failed, extracted root: %v\n", extracted_root))
		os.Exit(1)
	}
	if err != nil {
		panic(fmt.Sprintf("root extraction string parse error: %v\n", extracted_root))
		os.Exit(1)
	}

	// big.NewFloat() allowed here
	if x_big_float.Cmp(big.NewFloat(1)) == 1 {
		return find_decimal_radicand_gr_1(x_big_float, extracted_root, n)
	}
	return find_decimal_radicand_lt_1(x_big_float, extracted_root, n)
}

func find_decimal_radicand_gr_1(x, y *big.Float, n int) *big.Float {
	var y_to_n *big.Float = new(big.Float)
	y_to_n.Mul(y, y)
	for i := 2; i < n; i++ {
		y_to_n.Mul(y_to_n, y)
	}

	minus, _, _ := big.ParseFloat("-1", 10, Hausnum_prec, big.ToNearestEven)
	eq_l := y_to_n.Cmp(x)
	var tmp *big.Float = new(big.Float)
	tmp.Sub(y_to_n, x)
	if tmp.Signbit() {
		tmp.Mul(tmp, minus)
	}

	ten, _, _ := big.ParseFloat("10", 10, Hausnum_prec, big.ToNearestEven)
	new_y_ten, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)
	new_y_ten_to_n, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)

	new_y_ten.Mul(y, ten)
	new_y_ten_to_n.Mul(new_y_ten, new_y_ten)
	for i := 2; i < n; i++ {
		new_y_ten_to_n.Mul(new_y_ten_to_n, new_y_ten)
	}
	tmp2, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)
	tmp2.Sub(new_y_ten_to_n, x)
	if tmp2.Signbit() {
		tmp2.Mul(tmp2, minus)
	}
	eq_r := tmp.Cmp(tmp2)

	for eq_l == -1 && eq_r == 1 {
		y.Mul(y, ten)

		y_to_n.Mul(y, y)
		for i := 2; i < n; i++ {
			y_to_n.Mul(y_to_n, y)
		}

		//////////////////////////

		eq_l = y_to_n.Cmp(x)
		tmp.Sub(y_to_n, x)
		if tmp.Signbit() {
			tmp.Mul(tmp, minus)
		}

		new_y_ten.Mul(y, ten)
		new_y_ten_to_n.Mul(new_y_ten, new_y_ten)
		for i := 2; i < n; i++ {
			new_y_ten_to_n.Mul(new_y_ten_to_n, new_y_ten)
		}
		tmp2.Sub(new_y_ten_to_n, x)
		if tmp2.Signbit() {
			tmp2.Mul(tmp2, minus)
		}
		eq_r = tmp.Cmp(tmp2)
	}

	return y
}

func find_decimal_radicand_lt_1(x, y *big.Float, n int) *big.Float {
	var y_to_n *big.Float = new(big.Float)
	y_to_n.Mul(y, y)
	for i := 2; i < n; i++ {
		y_to_n.Mul(y_to_n, y)
	}

	minus, _, _ := big.ParseFloat("-1", 10, Hausnum_prec, big.ToNearestEven)
	eq_l := y_to_n.Cmp(x)
	var tmp *big.Float = new(big.Float)
	tmp.Sub(y_to_n, x)
	if tmp.Signbit() {
		tmp.Mul(tmp, minus)
	}

	ten, _, _ := big.ParseFloat("10", 10, Hausnum_prec, big.ToNearestEven)
	new_y_ten, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)
	new_y_ten_to_n, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)

	new_y_ten.Quo(y, ten)
	new_y_ten_to_n.Mul(new_y_ten, new_y_ten)
	for i := 2; i < n; i++ {
		new_y_ten_to_n.Mul(new_y_ten_to_n, new_y_ten)
	}
	tmp2, _, _ := big.ParseFloat("0", 10, Hausnum_prec, big.ToNearestEven)
	tmp2.Sub(new_y_ten_to_n, x)
	if tmp2.Signbit() {
		tmp2.Mul(tmp2, minus)
	}
	eq_r := tmp.Cmp(tmp2)

	for eq_l == 1 && eq_r == 1 {
		y.Quo(y, ten)

		y_to_n.Mul(y, y)
		for i := 2; i < n; i++ {
			y_to_n.Mul(y_to_n, y)
		}

		//////////////////////////

		eq_l = y_to_n.Cmp(x)
		tmp.Sub(y_to_n, x)
		if tmp.Signbit() {
			tmp.Mul(tmp, minus)
		}

		new_y_ten.Quo(y, ten)
		new_y_ten_to_n.Mul(new_y_ten, new_y_ten)
		for i := 2; i < n; i++ {
			new_y_ten_to_n.Mul(new_y_ten_to_n, new_y_ten)
		}
		tmp2.Sub(new_y_ten_to_n, x)
		if tmp2.Signbit() {
			tmp2.Mul(tmp2, minus)
		}
		eq_r = tmp.Cmp(tmp2)
	}

	return y
}

func check_real_string(str_real string) bool {
	if len(str_real) < 20 {
		panic(fmt.Sprintf("string of real number low precision error: %v\n", str_real))
		os.Exit(1)
	}
	/*
		// rather just WARNING //
		str_max_ix := len(str_real) - 1
		for ix := str_max_ix; ix > str_max_ix-10; ix-- {
			if str_real[ix] != '0' {
				log.Println(fmt.Sprintf("WARNING: string of real number low precision error: '%c' on index [%v] expected '0' (%v)\n", str_real[ix], ix, str_real))
			}
		}
		/////////////////////////
	*/

	return true
}

func find_character(str_real string, character byte) (int, bool) {
	if len(str_real) == 0 {
		return 0, false
	}

	str_max_ix := len(str_real) - 1
	ix := 0
	for ix < str_max_ix && str_real[ix] != character {
		ix++
	}

	if ix <= str_max_ix && str_real[ix] == character {
		return ix, true
	}

	return 0, false
}

func SplitStringMapUInt64(line string) []uint64 {
	row := strings.Split(line, " ")
	array := make([]uint64, len(row))
	for j := 0; j < len(array); j++ {
		value, err := strconv.ParseUint(row[j], 10, 64)
		Check_error(err)
		array[j] = value
	}
	return array
}

func SortedMap(unsortedMap map[uint64]uint64) []map[uint64]uint64 {
	keys := make(SortAbleUInt64Array, len(unsortedMap))
	ix := 0
	for key := range unsortedMap {
		keys[ix] = key
		ix++
	}
	sort.Sort(keys)

	sortedMap := make([]map[uint64]uint64, len(keys))
	for i := 0; i < len(sortedMap); i++ {
		sortedMap[i] = map[uint64]uint64{keys[i]: unsortedMap[keys[i]]}
	}

	return sortedMap
}
