package agreement

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
	"strings"
	"time"

	"packages/a_u_x"
	"packages/orca_verifier"
	results_pkg "packages/results"
)

const (
	comparison_group_log = "comparison_group_log.txt"
	big_float_precision  = a_u_x.Hausnum_prec // default 53
)

func init_networks(workingDirectory, inputListFileName string) (map[string]*Network, []string) {
	file, err := os.Open(workingDirectory + "/" + inputListFileName)
	defer file.Close()
	a_u_x.Check_error(err)

	networks := make(map[string]*Network)
	var networksFileNames []string

	index := uint8(0)
	br := bufio.NewReader(file)
	line, _, _ := br.ReadLine()
	for line != nil {
		if len(line) > 0 {
			networkData := strings.Fields(string(line))

			if len(networkData) < 1 {
				continue
			}

			networkFileName := networkData[0]
			networkGroupLabel := "null"
			if len(networkData) > 1 {
				networkGroupLabel = networkData[1]
			}

			networks[networkFileName] = &Network{File_name: networkFileName, Id: index, Group: networkGroupLabel}
			networksFileNames = append(networksFileNames, networkFileName)
			index++
		}
		line, _, _ = br.ReadLine()
	}

	return networks, networksFileNames

}

type Network struct {
	File_name            string
	Id                   uint8
	Group                string
	Agreement_arithmetic map[string]string
	Agreement_geometric  map[string]string
}

type Agreement struct {
	results *results_pkg.Results

	scaled_gdd     map[string]map[uint8]map[uint64]*big.Float
	total_area_gdd map[string]map[uint8]*big.Float
	normalized_gdd map[string]map[uint8]map[uint64]*big.Float

	agreements_computed map[string]*big.Float

	networks          map[string]*Network
	networksFileNames []string

	workingDirectory string
}

func NewAgreementFromOrca(workingDirectory, orcaOutputDirectory, inputListFileName string) *Agreement {
	agreement := new(Agreement)

	agreement.workingDirectory = workingDirectory
	agreement.networks, agreement.networksFileNames = init_networks(workingDirectory, inputListFileName)

	agreement.results = results_pkg.NewResults(agreement.networksFileNames)
	agreement.results.Init_results_from_orca()
	for _, networkFileName := range agreement.networksFileNames {
		orcaGDD := orca_verifier.NewOrcaGDD(workingDirectory, networkFileName, orcaOutputDirectory)
		agreement.results.Results[networkFileName] = orcaGDD.Gdd

	}
	log.Println("GDD Agreement input orbit distributions from ORCA merged and loaded.")

	agreement.scaled_gdd = make(map[string]map[uint8]map[uint64]*big.Float)
	agreement.total_area_gdd = make(map[string]map[uint8]*big.Float)
	agreement.normalized_gdd = make(map[string]map[uint8]map[uint64]*big.Float)

	agreement.agreements_computed = make(map[string]*big.Float)

	return agreement
}

func (a *Agreement) Start() {
	log.Println("Initialize GDD agreement...")
	for ix, networkFileName := range a.networksFileNames {
		log.Println(fmt.Sprintf("... %d / %d", ix+1, len(a.networksFileNames)))
		file_name := networkFileName

		a.scaled_gdd[file_name] = make(map[uint8]map[uint64]*big.Float)
		a.total_area_gdd[file_name] = make(map[uint8]*big.Float)
		a.normalized_gdd[file_name] = make(map[uint8]map[uint64]*big.Float)

		for o := uint8(0); o < 73; o++ {
			a.scaled_gdd[file_name][o] = make(map[uint64]*big.Float)
			a.total_area_gdd[file_name][o], _, _ = big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
			a.normalized_gdd[file_name][o] = make(map[uint64]*big.Float)

			for touch := range a.results.Results[file_name][o].Orbit_distribution {
				if touch >= 1 {
					big_value, _, _ := big.ParseFloat(strconv.FormatUint(a.results.Results[file_name][o].Orbit_distribution[touch], 10), 10, a_u_x.Hausnum_prec, big.ToNearestEven)
					big_touch, _, _ := big.ParseFloat(strconv.FormatUint(touch, 10), 10, a_u_x.Hausnum_prec, big.ToNearestEven)

					a.scaled_gdd[file_name][o][touch], _, _ = big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
					a.scaled_gdd[file_name][o][touch].Quo(big_value, big_touch)

					a.total_area_gdd[file_name][o].Add(a.total_area_gdd[file_name][o], a.scaled_gdd[file_name][o][touch])
				}
			}
			for touch := range a.results.Results[file_name][o].Orbit_distribution {
				if touch >= 1 {
					a.normalized_gdd[file_name][o][touch], _, _ = big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
					a.normalized_gdd[file_name][o][touch].Quo(a.scaled_gdd[file_name][o][touch], a.total_area_gdd[file_name][o])
				}
			}
		}
	}
	log.Println("Normalized distributions computed successfully!")

	if _, err := os.Stat(a.workingDirectory + "/" + comparison_group_log); !os.IsNotExist(err) {
		err := os.Remove(a.workingDirectory + "/" + comparison_group_log)
		a_u_x.Check_error(err)
	}
}

func (a *Agreement) distance(file_name_G, file_name_H string, orbit uint8) *big.Float {
	result, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	for touch_G := range a.normalized_gdd[file_name_G][orbit] {
		difference, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

		n_G, exists_G := a.normalized_gdd[file_name_G][orbit][touch_G]
		if !exists_G {
			n_G, _, _ = big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
		}

		n_H, exists_H := a.normalized_gdd[file_name_H][orbit][touch_G]
		if !exists_H {
			n_H, _, _ = big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
		}

		difference.Sub(n_G, n_H)

		squared, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
		squared.Mul(difference, difference)

		result.Add(result, squared)
	}
	for touch_H := range a.normalized_gdd[file_name_H][orbit] {
		if _, intersection_touch_key := a.normalized_gdd[file_name_G][orbit][touch_H]; !intersection_touch_key {
			difference, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
			difference.Copy(a.normalized_gdd[file_name_H][orbit][touch_H])

			squared, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
			squared.Mul(difference, difference)

			result.Add(result, squared)
		}

	}

	two, _, _ := big.ParseFloat("2", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	result = a_u_x.Big_float_nth_root(two, result, big_float_precision)

	sqrt2 := "1.4142135623730950488016887242096980785696718753769480731766797379907324784621070388503875343276415727350138462309122970249248360558507372126441214970999358314132226659275055927557999505011527820605714701095599716059702745345968620147285174186408891986095523292304843087143214508397626036279952514079896872533965463318088296406206152583523950547457502877599617299"
	big_sqrt2, _, _ := big.ParseFloat(sqrt2, 10, a_u_x.Hausnum_prec, big.ToNearestEven)
	result.Quo(result, big_sqrt2)

	return result
}

func (a *Agreement) Distance_reversed(file_name_G, file_name_H string, orbit uint8) *big.Float {
	result, _, _ := big.ParseFloat("1", 10, a_u_x.Hausnum_prec, big.ToNearestEven)
	result.Sub(result, a.distance(file_name_G, file_name_H, orbit))

	return result
}

func (a *Agreement) Agreement_arithmetic(file_name_G, file_name_H string) *big.Float {
	result, _, _ := big.ParseFloat("0", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	for o := uint8(0); o < 73; o++ {
		result.Add(result, a.Distance_reversed(file_name_G, file_name_H, o))
	}
	count, _, _ := big.ParseFloat("73", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	result.Quo(result, count)

	return result
}

func (a *Agreement) Agreement_geometric(file_name_G, file_name_H string) *big.Float {
	result, _, _ := big.ParseFloat("1", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	for o := uint8(0); o < 73; o++ {
		result.Mul(result, a.Distance_reversed(file_name_G, file_name_H, o))
	}

	count, _, _ := big.ParseFloat("73", 10, a_u_x.Hausnum_prec, big.ToNearestEven)

	result = a_u_x.Big_float_nth_root(count, result, big_float_precision)

	return result
}

func (a *Agreement) Add_get_agreement_computed(use_arithmetic_notgeometric bool, research_participant_G, research_participant_H string) *big.Float {
	analysis := a.get_agreement_method_type(use_arithmetic_notgeometric)

	agreement_key := analysis + " " + research_participant_G + " " + research_participant_H

	if research_participant_G <= research_participant_H {
		graph_G, graph_H := research_participant_G, research_participant_H
		start_time := time.Now()

		if !a.contains_agreement_computed(use_arithmetic_notgeometric, research_participant_G, research_participant_H) {
			var agreement_computed *big.Float

			if use_arithmetic_notgeometric {
				agreement_computed = a.Agreement_arithmetic(graph_G, graph_H)
			} else {
				agreement_computed = a.Agreement_geometric(graph_G, graph_H)
			}
			a.agreements_computed[agreement_key] = agreement_computed
		}

		elapsed_time := time.Since(start_time)
		log.Printf("(%v) Participants' fMRIs {%s} {%s} are in **[ %.6f ]** match in terms of %s GDD Agreement.\n",
			elapsed_time, research_participant_G, research_participant_H,
			a.agreements_computed[agreement_key], analysis)

		return a.agreements_computed[agreement_key]
	}
	return a.Add_get_agreement_computed(use_arithmetic_notgeometric, research_participant_H, research_participant_G)
}

func (a *Agreement) contains_agreement_computed(use_arithmetic_notgeometric bool, research_participant_G, research_participant_H string) bool {
	analysis := a.get_agreement_method_type(use_arithmetic_notgeometric)

	_, exists := a.agreements_computed[analysis+" "+research_participant_G+" "+research_participant_H]
	return exists
}

func (a *Agreement) get_agreement_method_type(use_arithmetic_notgeometric bool) string {
	if use_arithmetic_notgeometric {
		return "arithmetic"
	}
	return "geometric"
}

func (a *Agreement) Compare_all_networks(output_json_file_name string) {
	file, err := os.OpenFile(a.workingDirectory+"/"+comparison_group_log, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	defer file.Close()
	a_u_x.Check_error(err)

	progress_counter := 1
	network_count := len(a.networks)
	progress_total := network_count * network_count
	log.Printf("\n\nComparing all networks: %d jobs\n", progress_total)
	file.WriteString(fmt.Sprintf("\n\nComparing all networks: %d jobs\n", progress_total))

	sorted_networks := make(a_u_x.SortAbleStringArray, network_count)
	ix := 0
	for _, networkFileName := range a.networksFileNames {
		sorted_networks[ix] = networkFileName
		ix++
	}

	for _, network_G := range sorted_networks {
		a.networks[network_G].Agreement_arithmetic = make(map[string]string)
		a.networks[network_G].Agreement_geometric = make(map[string]string)

		for _, network_H := range sorted_networks {
			log.Printf("... %d / %d\n", progress_counter, progress_total)
			file.WriteString(fmt.Sprintf("... %d / %d\n", progress_counter, progress_total))

			agreement_arithmetic := a.Add_get_agreement_computed(true, network_G, network_H)
			agreement_geometric := a.Add_get_agreement_computed(false, network_G, network_H)

			// set output precision here e.g. %.6f
			a.networks[network_G].Agreement_arithmetic[network_H] = fmt.Sprintf("%.6f", agreement_arithmetic)
			a.networks[network_G].Agreement_geometric[network_H] = fmt.Sprintf("%.6f", agreement_geometric)

			file.WriteString(
				fmt.Sprintf("Networks {%s} {%s} are in **[ %.6f ]** match in terms of ARITHMETIC GDD Agreement.\n",
					network_G, network_H, agreement_arithmetic))
			file.WriteString(
				fmt.Sprintf("Networks {%s} {%s} are in **[ %.6f ]** match in terms of GEOMETRIC GDD Agreement.\n",
					network_G, network_H, agreement_geometric))

			progress_counter++
		}
	}

	file.Sync()

	// write results to JSON file
	log.Println("... Writing results to JSON file")
	json_file_path := a.workingDirectory + "/" + output_json_file_name

	if _, err := os.Stat(json_file_path); !os.IsNotExist(err) {
		err := os.Remove(json_file_path)
		a_u_x.Check_error(err)
	}

	json_file, err := os.Create(json_file_path)
	defer json_file.Close()
	a_u_x.Check_error(err)

	bytes, err := json.Marshal(a.networks)
	if err != nil {
		log.Printf("Can't convert results to json.")
		log.Printf("%v", err.Error())
		os.Exit(1)
	}

	n, err := json_file.Write(bytes)
	if err != nil {
		log.Printf("Can't save json results to disk, error: %v", err.Error())
		os.Exit(1)
	}
	expected_n := len(bytes)
	if n != expected_n {
		log.Printf("Can't save json results to disk, error: %d bytes were saved, but %d bytes should have been saved.", n, expected_n)
		os.Exit(1)
	}
	n, err = json_file.WriteString("\n")
	if err != nil {
		log.Printf("Can't separate saved json results on disk, error: %v", err.Error())
		os.Exit(1)
	}
	expected_n = len("\n")
	if n != expected_n {
		log.Printf("Can't separate saved json results on disk, error: %d bytes were saved, but %d bytes should have been saved.", n, expected_n)
		os.Exit(1)
	}

	json_file.Sync()
	log.Println("JSON file successfully created")
}
