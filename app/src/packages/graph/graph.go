package graph

import (
	"bufio"
	"os"
	"strconv"
	"strings"

	"packages/a_u_x"
	"packages/graph_isomorpher"
	vertex_pkg "packages/vertex"
)

type Graph struct {
	Vertices map[uint64]*vertex_pkg.Vertex
}

func NewGraph(file_name string) *Graph {
	file, err := os.Open(file_name)
	defer file.Close()
	a_u_x.Check_error(err)

	graph := new(Graph)
	graph.Vertices = make(map[uint64]*vertex_pkg.Vertex)
	gi := graph_isomorpher.NewGraphIsomorpher()

	br := bufio.NewReader(file)
	line, _, _ := br.ReadLine()
	for line != nil {
		if len(line) > 0 {
			edge := strings.Fields(string(line))
			v_id, _ := strconv.ParseUint(edge[0], 10, 64)
			n_id, _ := strconv.ParseUint(edge[1], 10, 64)

			v := graph.add_get_vertex(gi.Add_get_homomorphism_vertex(v_id))
			n := graph.add_get_vertex(gi.Add_get_homomorphism_vertex(n_id))
			v.Add_edge(n)
		}

		line, _, _ = br.ReadLine()
	}

	return graph
}

func (g *Graph) add_get_vertex(id uint64) *vertex_pkg.Vertex {
	if !g.contains_vertex(id) {
		g.Vertices[id] = vertex_pkg.NewVertex(id)
	}
	return g.Vertices[id]
}

func (g *Graph) contains_vertex(id uint64) bool {
	_, exists := g.Vertices[id]
	return exists
}

func (g *Graph) Max_degree() uint64 {
	var max_deg uint64 = 0
	for _, vertex := range g.Vertices {
		k := uint64(len(vertex.Neighbours))
		max_deg = a_u_x.Max(max_deg, k)
	}
	return max_deg
}
