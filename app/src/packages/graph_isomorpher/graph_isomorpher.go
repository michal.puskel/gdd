package graph_isomorpher

import (
	"log"
	"os"
)

type GraphIsomorpher struct {
	auto_inc_index uint64
	homomorphism   map[uint64]uint64
}

func NewGraphIsomorpher() *GraphIsomorpher {
	return &GraphIsomorpher{
		auto_inc_index: 0,
		homomorphism:   make(map[uint64]uint64),
	}
}

func (gi *GraphIsomorpher) Add_get_homomorphism_vertex(id uint64) uint64 {
	if !gi.contains_homomorphism_vertex(id) {
		gi.homomorphism[id] = gi.auto_inc_index
		gi.auto_inc_index++
		if gi.auto_inc_index == 0 {
			log.Printf("GraphIsomorpher auto_inc_index overflow error")
			os.Exit(1)
		}
	}
	return gi.homomorphism[id]
}

func (gi *GraphIsomorpher) contains_homomorphism_vertex(id uint64) bool {
	_, exists := gi.homomorphism[id]
	return exists
}
