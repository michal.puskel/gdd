package orbit_distribution

type OrbitDistribution struct {
	Orbit_distribution map[uint64]uint64
}

func NewOrbitDistribution(distribution map[uint64]uint64) *OrbitDistribution {
	return &OrbitDistribution{
		Orbit_distribution: distribution,
	}
}

func (od *OrbitDistribution) Equals(d *OrbitDistribution) bool {
	equals := true
	for k := range od.Orbit_distribution {
		equals = equals && od.Orbit_distribution[k] == d.Orbit_distribution[k]

		/*
			if od.Orbit_distribution[k] != d.Orbit_distribution[k] {
				log.Println("touch ", k)
				log.Println("values: ", od.Orbit_distribution[k], d.Orbit_distribution[k])
			}
		*/
	}
	for k := range d.Orbit_distribution {
		equals = equals && od.Orbit_distribution[k] == d.Orbit_distribution[k]

		/*
			if od.Orbit_distribution[k] != d.Orbit_distribution[k] {
				log.Println("touch ", k)
				log.Println("values: ", od.Orbit_distribution[k], d.Orbit_distribution[k])
			}
		*/
	}
	return equals
}

func (od *OrbitDistribution) Add(d *OrbitDistribution) {
	for k := range d.Orbit_distribution {
		od.Orbit_distribution[k] += d.Orbit_distribution[k]
	}
}
