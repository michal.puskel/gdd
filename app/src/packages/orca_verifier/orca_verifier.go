package orca_verifier

import (
	"bufio"
	"io"
	"os"

	"packages/a_u_x"
	"packages/orbit_distribution"
)

type OrcaGDD struct {
	Gdd map[uint8]*orbit_distribution.OrbitDistribution
}

func NewOrcaGDD(workingDirectory, orca_file, orca_directory string) *OrcaGDD {
	orcaGDD := new(OrcaGDD)

	orcaGDD.Gdd = make(map[uint8]*orbit_distribution.OrbitDistribution)
	for o := uint8(0); o < 73; o++ {
		orcaGDD.Gdd[o] = orbit_distribution.NewOrbitDistribution(map[uint64]uint64{})
	}

	orca_file_path := workingDirectory + "/" + orca_directory + "/" + orca_file
	orcaGDD.load_orbit_distributions(orca_file_path)

	return orcaGDD
}

func (orcaGDD *OrcaGDD) load_orbit_distributions(orca_file_path string) {
	file, err := os.Open(orca_file_path)
	defer file.Close()
	a_u_x.Check_error(err)

	br := bufio.NewReader(file)
	var bytes []byte

	for {
		bytes, _, err = br.ReadLine()
		if err == io.EOF {
			break
		}

		vertex_orbit_touches := a_u_x.SplitStringMapUInt64(string(bytes))
		for o := uint8(0); o < 73; o++ {
			orcaGDD.Gdd[o].Orbit_distribution[vertex_orbit_touches[o]] += 1
		}
	}
}
