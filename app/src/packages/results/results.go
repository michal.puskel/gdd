package results

import (
	"packages/orbit_distribution"
)

type Results struct {
	Results           map[string]map[uint8]*orbit_distribution.OrbitDistribution
	networksFileNames []string
}

func NewResults(networksFileNames []string) *Results {
	return &Results{
		Results:           make(map[string]map[uint8]*orbit_distribution.OrbitDistribution),
		networksFileNames: networksFileNames,
	}
}

func (r *Results) Init_results_from_orca() {
	for _, networkFileName := range r.networksFileNames {
		r.Init_one_result_from_orca(networkFileName)
	}
}

func (r *Results) Init_one_result_from_orca(file_name string) {
	r.Results[file_name] = make(map[uint8]*orbit_distribution.OrbitDistribution)

	for o := uint8(0); o < 73; o++ {
		r.Results[file_name][o] = orbit_distribution.NewOrbitDistribution(map[uint64]uint64{})
	}
}
