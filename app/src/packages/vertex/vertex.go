package vertex

type Vertex struct {
	Id         uint64
	Neighbours map[uint64]*Vertex
}

func NewVertex(id uint64) *Vertex {
	return &Vertex{
		Id:         id,
		Neighbours: make(map[uint64]*Vertex),
	}
}

func (v *Vertex) Is_adjacent(n *Vertex) bool {
	_, adjacent := v.Neighbours[n.Id]
	return adjacent
}

func (v *Vertex) Add_edge(n *Vertex) {
	if !v.Is_adjacent(n) && v.Id != n.Id {
		v.Neighbours[n.Id] = n
		n.Neighbours[v.Id] = v
	}
}

func (v *Vertex) Get_neighbours_array() []*Vertex {
	neighbours := make([]*Vertex, len(v.Neighbours))
	ix := 0
	for _, n := range v.Neighbours {
		neighbours[ix] = n
		ix++
	}
	return neighbours
}
